# Tender, Award and Contract Status Details Extension

In some cases, it is important to preserve the local name of the status in which
the tender, award or contract are. This extension adds the field statusDetails
to the tender, award and contract objecs in order to provide the local name 
of the particular status used.

## Example

```json
{
    "tender": {
        "status": "complete",
        "statusDetails": "Adjudicado"
    },
    "awards": [
        {
            "status": "active",
            "statusDetails": "Adjudicado"
        }
    ],
    "contracts": [
        {
         "status": "active",
         "statusDetails": "Adjudicado"
        }
    
    ]
}

```

## Issues

Report issues for this extension in the [ocds-extensions repository](https://github.com/open-contracting/ocds-extensions/issues), putting the extension's name in the issue's title.

## Changelog

This extension was originally discussed in <https://github.com/open-contracting/standard/issues/764>.